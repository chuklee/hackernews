import { NgModule } from '@angular/core';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';

const MATERIAL_MODULES = [
	MatButtonModule, MatIconModule, MatToolbarModule, MatSidenavModule
];

@NgModule({
	imports: MATERIAL_MODULES,
	exports: MATERIAL_MODULES
})
export class MaterialModule { }

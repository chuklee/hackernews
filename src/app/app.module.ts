import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';


import { MaterialModule } from './material.module';
import { AppComponent } from './app.component';
import { NewsListComponent } from './components/news-list.component';
import { SidemenuComponent } from './components/sidemenu.component';


@NgModule({
  declarations: [
    AppComponent,
    NewsListComponent,
    SidemenuComponent
  ],
  imports: [
    BrowserModule,
	BrowserAnimationsModule,
	MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
